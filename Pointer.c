#include<stdio.h>
void swap(int *x,int *y);
int main()
{
 int num1,num2;
 printf("enter two numbers\n");
 scanf("%d%d",&num1,&num2);
 printf("before swapping num1=%d,num2=%d\n",num1,num2);
 swap(&num1,&num2);
 printf("after swapping num1=%d,num2=%d\n",num1,num2);
}
 void swap(int *x,int *y)
{
  int t;
  t=*x;
  *x=*y;
  *y=t;
}
                        